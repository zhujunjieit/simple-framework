import request from '@/utils/request'

// 查询电识别结果列表
export function listReader(query) {
  return request({
    url: '/ai/reader/list',
    method: 'get',
    params: query
  })
}

// 查询电识别结果详细
export function getReader(aiReaderId) {
  return request({
    url: '/ai/reader/' + aiReaderId,
    method: 'get'
  })
}

// 新增电识别结果
export function addReader(data) {
  return request({
    url: '/ai/reader',
    method: 'post',
    data: data
  })
}

// 修改电识别结果
export function updateReader(data) {
  return request({
    url: '/ai/reader',
    method: 'put',
    data: data
  })
}

// 删除电识别结果
export function delReader(aiReaderId) {
  return request({
    url: '/ai/reader/' + aiReaderId,
    method: 'delete'
  })
}

// 导出电识别结果
export function exportReader(query) {
  return request({
    url: '/ai/reader/export',
    method: 'get',
    params: query
  })
}

// 导出电识别结果
export function exportImgReader(query) {
  return request({
    url: '/ai/reader/exportAllImg',
    method: 'get',
    params: query
  })
}

