package com.jeeos.framework.aspectj.lang.enums;

/**
 * 操作状态
 * 
 * @author jeeos
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
