package com.jeeos.common.exception.file;

import com.jeeos.common.exception.BaseException;

/**
 * 文件信息异常类
 * 
 * @author jeeos
 */
public class FileException extends BaseException
{
    private static final long serialVersionUID = 1L;

    public FileException(String code, Object[] args)
    {
        super("file", code, args, null);
    }

}
