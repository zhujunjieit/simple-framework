package com.jeeos.project.ai.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.jeeos.framework.aspectj.lang.annotation.Excel;
import com.jeeos.framework.web.domain.BaseEntity;

/**
 * 电识别结果对象 ai_reader
 * 
 * @author jeeos
 * @date 2020-02-10
 */
public class AiReader extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String aiReaderId;

    /** 电表类型 */
    @Excel(name = "电表类型")
    private String meterType;

    /** 识别结果（0识别正确 3仅数字正确 9识别错误） */
    @Excel(name = "识别结果", readConverterExp = "0=识别正确,3=仅数字正确,9=识别错误")
    private String state;

    /** 识别读数 */
    @Excel(name = "识别读数")
    private String number;

    /** 正确度数 */
    @Excel(name = "正确度数")
    private String rightNumber;

    /** 识别后图片 */
    @Excel(name = "识别后图片")
    private String readerImg;

    /** 原图 */
    @Excel(name = "原图")
    private String img;

    /** 文件名 */
    @Excel(name = "文件名")
    private String imgName;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setAiReaderId(String aiReaderId) 
    {
        this.aiReaderId = aiReaderId;
    }

    public String getAiReaderId() 
    {
        return aiReaderId;
    }
    public void setMeterType(String meterType) 
    {
        this.meterType = meterType;
    }

    public String getMeterType() 
    {
        return meterType;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }
    public void setNumber(String number) 
    {
        this.number = number;
    }

    public String getNumber() 
    {
        return number;
    }
    public void setRightNumber(String rightNumber) 
    {
        this.rightNumber = rightNumber;
    }

    public String getRightNumber() 
    {
        return rightNumber;
    }
    public void setReaderImg(String readerImg)
    {
        this.readerImg = readerImg;
    }

    public String getReaderImg()
    {
        return readerImg;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("aiReaderId", getAiReaderId())
            .append("meterType", getMeterType())
            .append("state", getState())
            .append("number", getNumber())
            .append("rightNumber", getRightNumber())
            .append("readerImg", getReaderImg())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
