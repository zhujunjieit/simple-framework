package com.jeeos.project.ai.service;

import com.jeeos.project.ai.domain.AiReader;

import java.io.IOException;
import java.util.List;

/**
 * 电识别结果Service接口
 * 
 * @author jeeos
 * @date 2020-02-10
 */
public interface IAiReaderService 
{
    /**
     * 查询电识别结果
     * 
     * @param aiReaderId 电识别结果ID
     * @return 电识别结果
     */
    public AiReader selectAiReaderById(String aiReaderId);

    /**
     * 查询电识别结果列表
     * 
     * @param aiReader 电识别结果
     * @return 电识别结果集合
     */
    public List<AiReader> selectAiReaderList(AiReader aiReader);

    /**
     * 新增电识别结果
     * 
     * @param aiReader 电识别结果
     * @return 结果
     */
    public int insertAiReader(AiReader aiReader);

    /**
     * 修改电识别结果
     * 
     * @param aiReader 电识别结果
     * @return 结果
     */
    public int updateAiReader(AiReader aiReader);

    /**
     * 批量删除电识别结果
     * 
     * @param aiReaderIds 需要删除的电识别结果ID
     * @return 结果
     */
    public int deleteAiReaderByIds(String[] aiReaderIds);

    /**
     * 删除电识别结果信息
     * 
     * @param aiReaderId 电识别结果ID
     * @return 结果
     */
    public int deleteAiReaderById(String aiReaderId);

    String zipAllImg(List<AiReader> list) throws IOException;
}
