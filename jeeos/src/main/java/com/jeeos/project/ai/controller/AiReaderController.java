package com.jeeos.project.ai.controller;

import java.io.IOException;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jeeos.framework.aspectj.lang.annotation.Log;
import com.jeeos.framework.aspectj.lang.enums.BusinessType;
import com.jeeos.project.ai.domain.AiReader;
import com.jeeos.project.ai.service.IAiReaderService;
import com.jeeos.framework.web.controller.BaseController;
import com.jeeos.framework.web.domain.AjaxResult;
import com.jeeos.common.utils.poi.ExcelUtil;
import com.jeeos.framework.web.page.TableDataInfo;

/**
 * 电识别结果Controller
 * 
 * @author jeeos
 * @date 2020-02-10
 */
@RestController
@RequestMapping("/ai/reader")
public class AiReaderController extends BaseController
{
    @Autowired
    private IAiReaderService aiReaderService;

    /**
     * 查询电识别结果列表
     */
    @PreAuthorize("@ss.hasPermi('ai:reader:list')")
    @GetMapping("/list")
    public TableDataInfo list(AiReader aiReader)
    {
        startPage();
        List<AiReader> list = aiReaderService.selectAiReaderList(aiReader);
        return getDataTable(list);
    }

    /**
     * 导出电识别结果列表
     */
    @PreAuthorize("@ss.hasPermi('ai:reader:export')")
    @Log(title = "电识别结果", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AiReader aiReader)
    {
        List<AiReader> list = aiReaderService.selectAiReaderList(aiReader);
        ExcelUtil<AiReader> util = new ExcelUtil<AiReader>(AiReader.class);
        return util.exportExcel(list, "reader");
    }

    /**
     * 导出电识别结果列表
     */
    @PreAuthorize("@ss.hasPermi('ai:reader:export')")
    @Log(title = "电识别结果", businessType = BusinessType.EXPORT)
    @GetMapping("/exportAllImg")
    public AjaxResult exportAllImg(AiReader aiReader) throws IOException {
        List<AiReader> list = aiReaderService.selectAiReaderList(aiReader);
        String filepath = aiReaderService.zipAllImg(list);
        return AjaxResult.success(filepath);
    }

    /**
     * 获取电识别结果详细信息
     */
    @PreAuthorize("@ss.hasPermi('ai:reader:query')")
    @GetMapping(value = "/{aiReaderId}")
    public AjaxResult getInfo(@PathVariable("aiReaderId") String aiReaderId)
    {
        return AjaxResult.success(aiReaderService.selectAiReaderById(aiReaderId));
    }

    /**
     * 新增电识别结果
     */
    @PreAuthorize("@ss.hasPermi('ai:reader:add')")
    @Log(title = "电识别结果", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AiReader aiReader)
    {
        return toAjax(aiReaderService.insertAiReader(aiReader));
    }

    /**
     * 修改电识别结果
     */
    @PreAuthorize("@ss.hasPermi('ai:reader:edit')")
    @Log(title = "电识别结果", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AiReader aiReader)
    {
        return toAjax(aiReaderService.updateAiReader(aiReader));
    }

    /**
     * 删除电识别结果
     */
    @PreAuthorize("@ss.hasPermi('ai:reader:remove')")
    @Log(title = "电识别结果", businessType = BusinessType.DELETE)
	@DeleteMapping("/{aiReaderIds}")
    public AjaxResult remove(@PathVariable String[] aiReaderIds)
    {
        return toAjax(aiReaderService.deleteAiReaderByIds(aiReaderIds));
    }
}
